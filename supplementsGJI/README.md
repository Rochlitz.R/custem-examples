# Supplementary material for GJI publication *3D inversion of semi-airborne EM data* 

- visualization scripts also require the SAEM package: https://github.com/LIAG-S2/SAEM 
- adjusting the Schleiz data in example 03 also requires SAEM, but can be skipped

## 01 Correction of time synchronization errors

- Additional information about the correction of time-synchronization errors in our recorded semi-airborne data from Schleiz, 2017
- This information complements our processing workflow presented by Becken et al. (2020) 
- The processed data using the concepts above are available as input data for the real-world example, see 03

## 02 Synthetic semi-airborne example

- Files for generating the meshes, synthetic data, and running the inversion
- Additional visualizations of data and misfit

## 03 Real-world example, Schleiz

- Real data used for inversion of the Schleiz area
- Files for generating the meshes, and running the inversion
- Additional visualizations of data and misfit

