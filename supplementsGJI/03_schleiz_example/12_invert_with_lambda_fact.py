#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: rrochlitz
"""

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen import meshgen_utils as mu
from custEM.inv.inv_utils import MultiFWD
from custEM.inv.inv_utils import coverage
import numpy as np
import pygimli as pg
import zipfile


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #   run Schleiz data inversion  # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly
p = 2

# specify model and mesh names and import data
invmod = 'p' + str(p)
invmesh = 'combined_inv_p' + str(p) + '_lambda_fact'
saemdata = np.load('data/Combined_every2BxBz.npz', allow_pickle=True)

# specify initial resistivity model and noise level for symlog transformation
sig_bg = 2e-3
noise_B = 2e-3

# %% mesh generation

# unzip digital elevation model
with zipfile.ZipFile("../02_synthetic_example/data/dem_schleiz.zip","r") as zf:
    zf.extractall("data")

# create world
M = BlankWorld(name=invmesh,
               x_dim=[-2e4, 2e4],
               y_dim=[-1e4, 1e4],
               z_dim=[-1e4, 1e4],
               preserve_edges=True,
               topo='data/dem_25.asc',
               inner_area_cell_size=5e3,
               easting_shift=-saemdata['origin'][0],
               northing_shift=-saemdata['origin'][1],
               rotation=-float(saemdata['rotation']),
               outer_area_cell_size=1e6,
               )

# define inner inversion volume
invpoly = np.array([[-2e3, -2e3, 0.],
                    [1.1e4, -2e3, 0.],
                    [1.1e4, 2e3, 0.],
                    [-2e3, 2e3, 0.]])

# create transmitter paths
txs = [mu.refine_path(saemdata['tx'][ti], length=50.) for ti in range(5)]

# build surface with Tx paths
M.build_surface(insert_line_tx=txs)

# add inversion domain to world and extend to 3D
M.add_inv_domains(-800., invpoly, x_frame=2e3, y_frame=2e3, z_frame=2e3,
                  cell_size=5e6)
M.build_halfspace_mesh()

# add receivers
if p == 1:
    r = 1.
elif p == 2:
    r = 20.

allrx = mu.resolve_rx_overlaps([data["rx"] for data in saemdata["DATA"]], r)
rx_tri = mu.refine_rx(allrx, r, 30.)
M.add_paths(rx_tri)

# add receiver locations to parameter file for all receiver patches
for rx in [data["rx"] for data in saemdata["DATA"]]:
    M.add_rx(rx)

M.extend_world(5., 10., 10.)
if p == 1:
    M.call_tetgen(tet_param='-pq1.2aA')
elif p == 2:
    M.call_tetgen(tet_param='-pq1.6aA')

###############################################################################
# %% run inversion

# set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=saemdata, sig_bg=sig_bg,
               n_cores=72, p_fwd=p, start_iter=0)
fop.setRegionProperties("*", limits=[1e-4, 1])

# set up inversion operator
inv = pg.Inversion()
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)
dT = pg.trans.TransSymLog(noise_B)
inv.dataTrans = dT

# run inversion with slightly decreasing lambda factor after each iteration
invmodel = inv.run(fop.measured, fop.errors, lam=20., lamFactor=0.8,
                   verbose=True, startModel=fop.sig_0, maxIter=21)

# save final conductivity vector
np.save(fop.inv_dir + 'final_inv_model.npy', invmodel)

# %% post-processing

# invmodel = np.load(inv.fop.inv_dir + 'final_inv_model.npy')

pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel
pgmesh['coverage'] = coverage(inv, invmodel)
pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')
