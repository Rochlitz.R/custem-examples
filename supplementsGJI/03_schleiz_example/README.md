# Real-world data - Example 2 in GJI publication

- information and plots for our results are available in the results_2022_10 directory
- our results can be reproduced by running the following files

## 01 prepare input data 

- can be skipped if SAEM package was not installed, inversion input data were added to this repository

## 02, 12, 22 run inversion

- run either with p1 on mesh with highly refined Rx or p2 on coarser mesh, provides comparable results
- "12": run p2 inversion with decreasing lambda by using a lamda factor 
- "22": run p2 inversion with not symlog-transfomed data 

## 03 view resistivity model

## 04 view misfits

