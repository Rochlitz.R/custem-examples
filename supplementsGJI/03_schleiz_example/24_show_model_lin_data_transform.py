# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from matplotlib.colors import LogNorm
from custEM.meshgen import meshgen_utils as mu
from matplotlib import cm, rcParams
import matplotlib as mpl


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot resistivity model along slices # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# specify to plot either p1 or p2 results
p = 2

# %% define plot parameters and import results

# make nice colorbar
class MplColorHelper:

  def __init__(self, cmap_name, start, stop):
    self.cmap_name = cmap_name
    self.cmap = plt.get_cmap(cmap_name)
    self.norm = mpl.colors.LogNorm(vmin=start, vmax=stop)
    self.scalarMap = cm.ScalarMappable(norm=self.norm, cmap=self.cmap)

  def get_rgb(self, val):
    return self.scalarMap.to_rgba(val)

# specify color bar parameters
cmap = 'Spectral'
COL = MplColorHelper(cmap, 5., 5000.)

# load final resistivity model
path = './inv_results/p' + str(p) + '_combined_inv_p' + str(p) + '_lin_trans/'
mesh = pv.read(path + 'p' + str(p) + '_final_invmodel.vtk')

# %% plot results

# specify values to plot slices along rotated plot axis (x also possible)
normal = 'y'
scalars = 'res'
offsets = np.linspace(-1500., 1500., 7)[::-1]

# create figure
fig, ax = plt.subplots(7, 2, figsize=[12, 16], sharex=True, sharey=True)
for j, yoff in enumerate(offsets):
    # slice through model
    data = mesh.slice(normal=normal, origin=[0., yoff, 0.],
                      generate_triangles=True)

    # get triangulation
    x = data.points
    tri = data.faces.reshape((-1, 4))[:, 1:]
    u = data[scalars]
    cov = data["coverage"]
    cov = cov / np.max(cov)  # scale so that maximum value is 1.

    if normal == 'x':
        a, b = x[:, 1], x[:, 2] # yz
    elif normal == 'y':
        a, b = x[:, 0], x[:, 2] # xz
    elif normal == 'z':
        a, b = x[:, 0], x[:, 1] # xy

    # plot resistivities
    fig = ax[j, 0].tripcolor(a, b, tri, u, rasterized=True, cmap=cmap,
                          norm=LogNorm(vmin=5., vmax=5000.))

    fig2 = ax[j, 1].tripcolor(a, b, tri, cov, rasterized=True, cmap='Greys',
                             norm=LogNorm(vmin=1e-6, vmax=1e-0))

    for k in range(2):
        # adjust axes and labels
        ax[j, k].set_aspect('equal')
        ax[j, k].set_xlim([-2000., 11000.])
        ax[j, k].set_ylim([-1000., 500.])
        if j == 6:
            ax[j, k].set_xlabel("x' (m)", fontsize=12)
        ax[j, 0].set_ylabel("z (m)", fontsize=12)
        ax[j, 0].text(-1800., -800., "y' = " + str(yoff) + ' m', fontsize=10)

# add title and colorbar
plt.suptitle('Recovered resistivity model: p' + str(p))
cbar = plt.colorbar(fig, ax=ax, orientation='horizontal')
cbar.set_label(r'Resistivity ($\Omega$m)')
cbar.ax.tick_params(labelsize=12)
cbar = plt.colorbar(fig2, ax=ax, orientation='horizontal')
cbar.set_label(r'Coverage')
cbar.ax.tick_params(labelsize=12)

# save figure
plt.savefig('lintrans_invmodel_p' + str(p) + '.pdf', bbox_inches='tight')