# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #  run synthetic data inversion # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly
p = 1
invmod = 'p' + str(p)
invmesh = 'schleiz_inv_p' + str(p)

# %% create mesh
param = np.load('data/survey_parameters.npz', allow_pickle=True)

# define inversion area to be incorporated in the mesh
invpoly = np.array([[-3e3, -2e3, 0.],
                    [4e3, -2e3, 0.],
                    [4e3, 2e3, 0.],
                    [-3e3, 2e3, 0.]])

# shift and rotate inversion area to match footprint of Tx and Rx
invpoly = mu.rotate(invpoly, np.deg2rad(param['rot']))
invpoly[:, 0] += 800.
invpoly[:, 1] -= 400.

# create world
M = BlankWorld(name=invmesh,
               x_dim=[-1e4, 1e4],
               y_dim=[-1e4, 1e4],
               z_dim=[-1e4, 1e4],
               t_dir='data',
               topo='dem_25.asc',
               easting_shift=float(param['xshift']),
               northing_shift=float(param['yshift']),
               inner_area_cell_size=6e3,
               preserve_edges=True,
               )

# define surface refinements
if p == 1:
    rx_surf_tri = mu.refine_rx(param['rx_surf'], 0.5, 30.)
    txs = []
    for ti, tx in enumerate(param['txs']):
        txs.append(mu.refine_path(tx, length=5.))
elif p == 2:
    rx_surf_tri = mu.refine_rx(param['rx_surf'], 10., 30.)
    txs = param['txs']
else:
    print('Error! p must be 1 or 2! Aborting  ...')
    raise SystemExit

# build surface mesh
M.build_surface(insert_line_tx=txs,
                insert_paths=rx_surf_tri)

# define inversion domain
M.add_inv_domains(-500., invpoly, x_frame=1.4e3, y_frame=1.6e3, z_frame=2e3,
                  cell_size=1e7)

# build surface mesh and extension to 3D
M.build_halfspace_mesh()

rx_surf = param['rx_surf']
# add surface Rx coordinates with correct height to paramter file
rx_surf[:, 2] = M.get_topo_vals(rx_surf, z=-1.)
M.add_rx(rx_surf)

# shift airborne Rx coordinates to 60m height above triangulated surface
# choose proper refinement sizes for p1 or p2
rx_air = param['rx_air']
rx_air[:, 2] = M.get_topo_vals(rx_air, z=60.)

if p == 1:
    rx_common, rx_single, rx_tri = mu.refine_adaptive(
        rx_air, param['txs'], r1=2., r2=0.5, r3=0.1)
else:
    rx_common, rx_single, rx_tri = mu.refine_adaptive(
        rx_air, param['txs'], r1=20., r2=5., r3=1.)

# add airborne Rx coordinates to parameter file and add refinement triangles
M.add_rx(rx_common)
for ti in range(len(param['txs'])):
    M.add_rx(rx_single[ti])
M.add_paths(rx_tri)

# add boundary mesh and mesh generation with proper quality for p1 or p2
M.extend_world(5., 5., 5.)
if p == 1:
    M.call_tetgen(tet_param='-pq1.2aA')
else:
    M.call_tetgen(tet_param='-pq1.6aA')

# %% define inversion parameter

sig_bg = 2e-3
max_iter = 21
synth_data = np.load('data/ExyBxyzToImport.npz', allow_pickle=True)

# %% set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=synth_data,
               sig_bg=sig_bg, p_fwd=p, max_procs=6)
fop.setRegionProperties("*", limits=[1e-4, 1e0])

# %% set up inversion operator
inv = pg.Inversion(verbose=True)  # , debug=True)
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)
dT = pg.trans.TransSymLog(1e-3)
inv.dataTrans = dT

# %% run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=10., verbose=True,
                   startModel=fop.sig_0, maxIter=max_iter)

# save final conductivity vector
np.save(fop.inv_dir + 'final_inv_model.npy', invmodel)

# %% post-processing

# invmodel = np.load(inv.fop.inv_dir + 'final_inv_model.npy')

pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel

try:
    from saem.tools import coverage
    pgmesh['coverage'] = coverage(inv)
except ImportError:
    print("SAEM tools not found, coverage could not be computed")
    pass


pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')
