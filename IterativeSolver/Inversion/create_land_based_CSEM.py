# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
Adjusted by Weiss.M
"""


import numpy as np
import matplotlib.pyplot as plt
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import os
from saem import CSEMSurvey
import zipfile

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #  create synthetic data for inversion  # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# fix random seeed for reproducibility
np.random.seed(99999)

# %% mesh and survey parameter definitions

# unzip digital elevation model
with zipfile.ZipFile("data/dem_schleiz.zip","r") as zip_ref:
    zip_ref.extractall("data")

# origin of computational domain
xshift = - 701980
yshift = - 5604726

# rotation for synthetic Rx grid to match approximately with Tx direction
rot = -37.

# origianl Tx definitions (including rotation)
tx0 = np.array([[700884.6, 5604387.0, 0.0],
                [701003.2, 5604522.3, 0.0],
                [701118.7, 5604816.2, 0.0],
                [701526.1, 5605047.3, 0.0],
                [701983.0, 5605484.6, 0.0],
                [702313.4, 5605740.1, 0.0],
                [702452.2, 5605920.3, 0.0]])

tx1 = np.array([[703023.1, 5602890.3, 0.0],
                [703332.3, 5603164.9, 0.0],
                [703537.4, 5603341.4, 0.0],
                [703670.6, 5603451.0, 0.0],
                [703825.8, 5603672.3, 0.0],
                [704045.8, 5603830.1, 0.0],
                [704190.9, 5603911.3, 0.0],
                [704390.6, 5604007.0, 0.0]])

# shift Tx
txs = [tx0,tx1]
for ti, tx in enumerate(txs):
    txs[ti] = mu.refine_path(tx, length=20.)
    txs[ti][:, 0] += xshift
    txs[ti][:, 1] += yshift

# define airborne Rx
l=11
xr = np.linspace(-2500., 3500., 11)

rx_surf_h = np.zeros(13)
rx_surf_h[:] = np.linspace(-1500., 4500., 13)
rx_surf_h = np.delete(rx_surf_h, [2, 7], 0)

yr = np.linspace(-2000., 2000., 7)
#l=7
#yr = [-1600,-1200,-400,0,400,1200,1600]
yr = [-2000,-1600,-1200,-900,-400,0,400,900,1200,1600,2000]
#print(yr)
rx_air = np.zeros((11 * l, 3))

# create Rx grid
a = 0
for x in rx_surf_h:
#for x in xr:
    for y in yr:
        rx_air[a, 0] = x
        rx_air[a, 1] = y
        a += 1


# rotate and shift Rx grid above Tx
rx_air = mu.rotate(rx_air, np.deg2rad(rot))
rx_air[:, 1] -= 70.
rx_air[:, 0] += 70.

#print(rx_air)

rx_surf=rx_air

# define Rx

# rotate and shift Rx grid above Tx

#rx_surf = mu.rotate(rx_surf, np.deg2rad(rot))
#rx_air[:, 0] += 800.
#rx_air[:, 1] -= 400.

# define surface Rx for E-field recordings
# 1 line

# define surface Rx for E-field recordings
# 1 line
#rx_surf = np.zeros((11, 3))
#rx_surf_h = np.zeros((13, 3))
#rx_surf_h[:, 0] = np.linspace(-1500., 4500., 13)
#rx_surf_h = np.delete(rx_surf_h, [2, 7], 0)

#rx_surf = mu.rotate(rx_surf, np.deg2rad(rot))

#rx_air = rx_surf

# 3 lines
#l=3
#rx_surf = np.zeros((11*l, 3))
#rx_surf_h = np.zeros(13)
#rx_surf_h[:] = np.linspace(-1500., 4500., 13)
#rx_surf_h = np.delete(rx_surf_h, [2, 7])

#d=np.ones(11)
#rx_surf[0:11,0]=rx_surf_h
#rx_surf[0:11,1]=-d*400

#rx_surf[11:22,0]=rx_surf_h

#rx_surf[22:33,0]=rx_surf_h
#rx_surf[22:33,1]=d*400

#rx_surf = mu.rotate(rx_surf, np.deg2rad(rot))

#rx_air = rx_surf
  
# 5 lines
#l=5
#rx_surf = np.zeros((11*l, 3))
#rx_surf_h = np.zeros(13)
#rx_surf_h[:] = np.linspace(-1500., 4500., 13)
#rx_surf_h = np.delete(rx_surf_h, [2, 7])

#d=np.ones(11)
#rx_surf[0:11,0]=rx_surf_h
#rx_surf[0:11,1]=-d*1600

#rx_surf[11:22,0]=rx_surf_h
#rx_surf[11:22,1]=-d*400

#rx_surf[22:33,0]=rx_surf_h

#rx_surf[33:44,0]=rx_surf_h
#rx_surf[33:44,1]=d*400

#rx_surf[44:55,0]=rx_surf_h
#rx_surf[44:55,1]=d*1600

#rx_surf = mu.rotate(rx_surf, np.deg2rad(rot))

#rx_air = rx_surf

# 7 lines
#l=7
#rx_surf = np.zeros((11*l, 3))
#rx_surf_h = np.zeros(13)
#rx_surf_h[:] = np.linspace(-1500., 4500., 13)
#rx_surf_h = np.delete(rx_surf_h, [2, 7])

#d=np.ones(11)
#rx_surf[0:11,0]=rx_surf_h
#rx_surf[0:11,1]=-d*2500

#rx_surf[11:22,0]=rx_surf_h
#rx_surf[11:22,1]=-d*1200

#rx_surf[22:33,0]=rx_surf_h
#rx_surf[22:33,1]=-d*400

#rx_surf[33:44,0]=rx_surf_h

#rx_surf[44:55,0]=rx_surf_h
#rx_surf[44:55,1]=d*400

#rx_surf[55:66,0]=rx_surf_h
#rx_surf[55:66,1]=d*1200

#rx_surf[66:77,0]=rx_surf_h
#rx_surf[66:77,1]=d*2500

#rx_surf = mu.rotate(rx_surf, np.deg2rad(rot))

#rx_air = rx_surf

#print(rx_surf)

# define refinement triangles
rx_surf_tri = mu.refine_rx(rx_surf, 5., 30.)


# save survey parameters to import for inversion mesh generation

np.savez('data/survey_parameters.npz',
         xshift=xshift,
         yshift=yshift,
         rot=rot,
         txs=np.array(txs, dtype=object),
         rx_air=rx_air,
         rx_surf=rx_surf)


# %% mesh generation
# define poylgones for anomalies which outcrop at the surface
anom1 = np.array([[-2000., -500., 0.],
                  [-500., -1500., 0.],
                  [0., -1000., 0.],
                  [-800., 0., 0.],
                  [-1200., 200., 0.]])
anom2 = np.array([[-1200., 200., 0.],
                  [-800., 0., 0.],
                  [-300., 400., 0.],
                  [-500., 600., 0.]])
anom3 = np.array([[-1000., 800., 0.],
                  [300., 500., 0.],
                  [500., 1000., 0.],
                  [-800., 1600., 0.]])
anom4 = np.array([[1200., -3200., 0.],
                  [2200., -3600., 0.],
                  [3800., -400., 0.],
                  [3600., 200., 0.]])

# create world
M = BlankWorld(name='schleiz_synth_landCSEM',
               x_dim=[-5e3, 6e3],
               y_dim=[-6e3, 5e3],
               z_dim=[-5e3, 5e3],
               t_dir='data',
               topo='dem_25.asc',
               easting_shift=xshift,
               northing_shift=yshift,
               inner_area_size=[3400., 3000.],
               inner_area_shift=[800., -800.],
               inner_area='box',
               inner_area_cell_size=2e3,
               outer_area_cell_size=1e4,
               split_subsurface_anomalies=[True, False, False, False],
               z_approx=500.,
               preserve_edges=True,
               )

# define surface mesh
M.build_surface(insert_line_tx=txs,
                insert_paths=rx_surf_tri)

# define anomaly outcrops
d = [-600., -200., -2600., -1200.]
M.add_surface_anomaly(insert_paths=[anom1, anom2, anom3, anom4],
                      depths=d,
                      cell_sizes=[1e7, 1e6, 1e7, 1e7],
                      dips=[0., 0., 30., 50.],
                      dip_azimuths=[0., 0., 0., -70.],
                      split_depths=[[-200., -500., -1000.],
                                    [-200.],
                                    [-500.],
                                    [-800.]])

# build surface mesh and extension to 3D
M.build_halfspace_mesh()

# add 5th anomaly at greater depths in the center
M.add_plate(dx=2200., dy=600., dz=400., origin=[1000.0, -1200.0, -400.0],
            dip=30., dip_azimuth=rot-90., cell_size=1e6)

# add surface Rx coordinates with correct height to paramter file
rx_surf[:, 2] = M.get_topo_vals(rx_surf, z=-1.)
M.add_rx(rx_surf)

# shift airborne Rx coordinates to 60m height above triangulated surface
rx_air[:, 2] = M.get_topo_vals(rx_air, z=-1.)
M.add_rx(rx_air)
#rx_common, rx_single, rx_tri = mu.refine_adaptive(rx_air, txs)

# add airborne Rx coordinates to parameter file and add refinement triangles
#M.add_rx(rx_common)
#for ti in range(len(txs)):
#    M.add_rx(rx_single[ti])
#M.add_paths(rx_tri)

# add boundary mesh and mesh generation
M.extend_world(10., 10., 10.)
M.call_tetgen(tet_param='-pq1.6aA', suppress='')


# %% set synthetic data generation parameters

err = 0.05        #                           mu_0         nT
noise_B = 1e-3    # related to H (A/m) by 4 * pi * 1e-7 * 1e9 ~= 1256
noise_E = 1e-7    # long recordings, very good data quality

mesh = 'schleiz_synth_landCSEM'
p_fwd = 2         # polynomial order for forward modeling
mod = 'synth_data_landCSEM'
n_cores=72        # total number of cores used
min_freqs = None  # set to 2, 3 or 4 to trade time vs. RAM requirements,
#                 # e.g., 3 means each thread calculates 3 freqs,
#                 # so time is tripled but RAM requirements are only 1/3

freqs = [6., 10., 20., 35., 60., 100., 200., 350., 600., 1000., 2000., 3500.]
freqs = [6.]
#cmps = [['Ex', 'Ey'], ['Bx', 'By', 'Bz'],
#        ['Bx', 'By', 'Bz'], ['Bx', 'By', 'Bz']]
#tx_ids = [[0, 1], [0, 1], [0], [1]]

cmps = [['Ex', 'Ey'], ['Bx', 'By', 'Bz']]
tx_ids = [[0,1], [0,1]]

# for anomaly markers 2, 3, 4, 5, 6, respectively
sig_m = [5e-4, 5e-4, 5e-4, 2e-2, 2e-4, 5e-2, 1e-1]
# for marker 1, surrounding halfspace
sig_bg = 2e-3

# %% create synthetic data
#solvingapproach=['iterative','PRESB','iterative','H_iterative']
solvingapproach=['direct','MUMPS','directly','H_direct']

pfname = 'ExyBxyz'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores,solvingapproach=solvingapproach)
data = fop.response(sig_m)
np.save('data/' + pfname + '_no_noise.npy', data)

data = np.load('data/' + pfname + '_no_noise.npy')
# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_B

# overwrite abs_error for E-field recordings
half=0
ne = 2 * 2 * 12 * 11 * l   # 2 Tx * 2 Ex,Ey * 12 freqs * 11 E-field rx pos * l lines
half = int(len(data)/2)
abs_error[:ne] = np.abs(data[:ne]) * err + noise_E
abs_error[half:ne+half] = np.abs(data[half:ne+half]) * err + noise_E

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes for E_real, B_real, E_imag, B_imag
data[:ne][np.abs(data[:ne]) < noise_E*0.5] = np.nan
data[ne:half][np.abs(data[ne:half]) < noise_B*0.5] = np.nan
data[half:ne+half][np.abs(data[half:ne+half]) < noise_E*0.5] = np.nan
data[half+ne:][np.abs(data[half+ne:]) < noise_B*0.5] = np.nan

print(len(data))
print(data)

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)