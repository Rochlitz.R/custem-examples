import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
from matplotlib.ticker import FixedLocator, FixedFormatter

############ Prepare data #############
def time_to_sec(t):
   t=t.replace('.',':') 
   h, m, s = map(float, t.split(':'))
   return h * 60 + m + s/100.0

# Read in relevant data
f=open('readyforread.txt',"r")
lines=f.readlines()
result=[]
for x in lines:
    result.append(x.split()[1])
f.close()

timecoarse=[x.split()[10] for x in open('readyforread.txt').readlines()]
memcoarse=[x.split()[5] for x in open('readyforread.txt').readlines()]


# Define number of MPI processes used
n=2

# Convert string to float and time to seconds
j=0
for i in memcoarse:
    l=float(memcoarse[j])
    memcoarse[j]=l
    l=time_to_sec(timecoarse[j])
    timecoarse[j]=l
    j=j+1

timeinter=[x.split()[10] for x in open('readyforread1.txt').readlines()]
meminter=[x.split()[5] for x in open('readyforread1.txt').readlines()]

nncoarse=j/n

# Convert string to float and time to seconds
j=0
for i in meminter:
    l=float(meminter[j])
    meminter[j]=l
    l=time_to_sec(timeinter[j])
    timeinter[j]=l
    j=j+1

timefine=[x.split()[10] for x in open('readyforread2.txt').readlines()]
memfine=[x.split()[5] for x in open('readyforread2.txt').readlines()]

nninter=j/n

# Convert string to float and time to seconds
j=0
for i in memfine:
    l=float(memfine[j])
    memfine[j]=l
    l=time_to_sec(timefine[j])
    timefine[j]=l
    j=j+1    

nnfine=j/n    

# Reshape arrays and sum up memory of processes
# Take average time of processes and normalise

# convert list to numpy array
tmp=np.reshape(memcoarse, (int(nncoarse),n))
for i in range(n):
   for j in range(int(nncoarse)):
      if tmp[j,i] > 1e5:
         tmp[j,i]=tmp[j,i]/1e6
Rescoarse=np.sum(tmp,axis=1).tolist() 

tmp=np.reshape(timecoarse, (int(nncoarse),n))
rtcoarse=np.mean(tmp, axis=1)
rtcoarse=np.divide(rtcoarse,max(rtcoarse))


tmp=np.reshape(meminter, (int(nninter),n))
for i in range(n):
   for j in range(int(nninter)):
      if tmp[j,i] > 1e5:
         tmp[j,i]=tmp[j,i]/1e6
Resinter=np.sum(tmp,axis=1).tolist()   

tmp=np.reshape(timeinter, (int(nninter),n))
rtinter=np.mean(tmp, axis=1)
rtinter=np.divide(rtinter,max(rtinter))


tmp=np.reshape(memfine, (int(nnfine),n))
for i in range(n):
   for j in range(int(nnfine)):
      if tmp[j,i] > 1e5:
         tmp[j,i]=tmp[j,i]/1e6
Resfine=np.sum(tmp,axis=1).tolist()  

tmp=np.reshape(timefine, (int(nnfine),n))
rtfine=np.mean(tmp, axis=1)
rtfine=np.divide(rtfine,max(rtfine))


###################### PLOTS ################

# Plot
cm = 1/2.54  # centimeters in inches
fig1, ax1 = plt.subplots(figsize=(12*cm, 12*cm))

# Display grid
plt.grid(True, which ="both",linestyle=":")
  
# Linear X axis, Logarithmic Y axis
ax1.plot(rtcoarse, Rescoarse,'b-..' )
ax1.plot(rtinter, Resinter, 'r^--')
ax1.plot(rtfine, Resfine, 'g:+')

ax1.legend(['Coarse: Peak memory='+str(round(max(Rescoarse),2)), 'Intermediate: Peak memory='+str(max(Resinter)), 'Fine: Peak memory='+str(max(Resfine))], loc ="upper left")

maxcoarse=str(max(Rescoarse))
maxinter=str(max(Resinter))
maxfine=str(max(Resfine))
          
# Provide the title for the semilogy plt
plt.title('Memory usage over normalised time')
  
# Give y axis label for the semilogy plt
plt.ylabel('Memory used [GB]')
  
# Give x axis label for the semilogy plt
plt.xlabel('Normalised time')

# Set axis limits
plt.ylim([0, 35])
plt.xlim([0, 1])

# Save plt in eps or other format
#plt.savefig('MemoryUsageOverTime.eps', format='eps', dpi=1200,bbox_inches="tight")
#plt.savefig('MemoryUsageOverTime.svg', format='svg', dpi=1200,bbox_inches="tight")
plt.savefig('MemoryUsageOverTime.pdf', format='pdf', dpi=1200,bbox_inches="tight")

# Display the semilogy plt
plt.show()


##### Plot memory vs degrees of freedom ######################

cm = 1/2.54  # centimeters in inches
fig2, ax2 = plt.subplots(figsize=(12*cm, 12*cm))

# Display grid
plt.grid(True, which ="both",linestyle=":")

size=np.array([694948, 5948746, 13447978])
peakmem=[max(Rescoarse),max(Resinter),max(Resfine)]

modelx=np.linspace(500000,15000000,100000)
mem_ideal=np.divide(modelx,size[0])*peakmem[0]

# Linear X axis, Logarithmic Y axis
ax2.loglog(size,peakmem, 'bs' )
ax2.loglog(modelx,mem_ideal,'b:' )

ax2.legend(["Observed scaling","Scaling of peak memory consumption $\propto$ DOF"], loc ="lower left",fontsize=9)

# Provide the title for the semilogy plt
plt.title('Peak Memory vs # Degrees of freedom')
  
# Give y axis label for the semilogy plt
plt.ylabel('Peak Memory [GB]')
  
# Give x axis label for the semilogy plt
plt.xlabel('Degrees of freedom (DOF)')

# Set axis limits
plt.ylim([1, 30])
plt.xlim([500000, 15000000])

# Set custom ticks
x_formatter = FixedFormatter(["694'948", "5'948'746","13'447'978"])
x_locator = FixedLocator(size)
ax2.xaxis.set_major_formatter(x_formatter)
ax2.xaxis.set_major_locator(x_locator)

y_formatter = FixedFormatter([str(round(max(Rescoarse),2)),str(max(Resinter)),str(max(Resfine))])
y_locator = FixedLocator(peakmem)
ax2.yaxis.set_major_formatter(y_formatter)
ax2.yaxis.set_major_locator(y_locator)


# Save plt in eps or other format
#plt.savefig('LaptopMemvsSize.eps', format='eps', dpi=1200,bbox_inches="tight")
#plt.savefig('LaptopMemvsSize.svg', format='svg', dpi=1200,bbox_inches="tight")
plt.savefig('LaptopMemvsSize.pdf', format='pdf', dpi=1200,bbox_inches="tight")

# Display the semilogy plt
plt.show()
