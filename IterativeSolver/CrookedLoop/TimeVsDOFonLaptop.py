import matplotlib.pyplot as plt
import matplotlib.ticker
from matplotlib.ticker import FixedLocator, FixedFormatter
import numpy as np
import math

t=[]
with open('timings.txt', 'r') as fobj:
    for line in fobj:
        t.append(float(line))
    

fobj.close()

size=np.array([694948, 5948746, 13447978])

modelx=np.linspace(500000,15000000,100000)
t_ideal=np.divide(modelx,size[0])*t[0]

cm = 1/2.54  # centimeters in inches
fig1, ax1 = plt.subplots(figsize=(12*cm, 12*cm))

# Display grid
plt.grid(True, which ="both",linestyle=":")

# Linear X axis, Logarithmic Y axis
ax1.loglog(size,t, 'bs' )
ax1.loglog(modelx,t_ideal, 'b:')

ax1.legend(["Observed scaling","Scaling of run time $\propto$ DOF"], loc ="lower right")

# Provide the title for the semilogy plt
plt.title('Execution time vs # Degrees of freedom')
  
# Give y axis label for the semilogy plt
plt.ylabel('Total run time [s]')
  
# Give x axis label for the semilogy plt
plt.xlabel('Degrees of freedom (DOF)')

# Set axis limits
plt.ylim([180, 6000])
plt.xlim([500000, 15000000])

# Set custom ticks
x_formatter = FixedFormatter(["694'948", "5'948'746","13'447'978"])
x_locator = FixedLocator(size)
ax1.xaxis.set_major_formatter(x_formatter)
ax1.xaxis.set_major_locator(x_locator)

y_formatter = FixedFormatter([str(t[0]), str(t[1]), str(t[2])])
y_locator = FixedLocator(t)
ax1.yaxis.set_major_formatter(y_formatter)
ax1.yaxis.set_major_locator(y_locator)

# Save plt in eps or other format
#plt.savefig('LaptopTimesvsSize.eps', format='eps', dpi=1200,bbox_inches="tight")
#plt.savefig('LaptopTimesvsSize.svg', format='svg', dpi=1200,bbox_inches="tight")
plt.savefig('LaptopTimesvsSize.pdf', format='pdf', dpi=1200,bbox_inches="tight")

# Display the semilogy plt
plt.show()
