################################################################################
# # # # #                  frequency-domain Example 2                  # # # # #
################################################################################

   crooked loop source on three-layered subsuface using the iterative solver

################################################################################

# Author: Michael Weiss, March 2024

# Run ReproduceFigures.sh in the directory for modelling example 2 of custEM-examples
# This bash script reproduces the figures in directory ExampleFigures
# Required scripts: run.sh, PlotMemory.sh, TimeVsDOFonLaptop.py,
#                   MemVSDOFonLaptop.py


# 0) Run mesh_example_2.py first (python mesh_example_2.py)
# 1) Make *.sh executable using chmod +x *.sh in terminal
# 2) Adjust "username" when calling grep in run.sh

# Run three simulations with various grids
# Important: make sure to set meshes=['null'] in run_example_2.py
# Note: Simulations are run with TWO MPI PROCESSES (may be changed by setting
#       mpirun -n x python run_example_2.py x to desired number of MPI processes)


# The program takes about TWO hours to finish.

################################################################################