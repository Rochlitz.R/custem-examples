# Run this script in the directory for modelling example 2 of custEM-examples
# This bash script reproduces the figures in directory Figures
# To be run in directory for
# Required scripts: run.sh, PlotMemory.sh, TimeVsDOFonLaptop.py,
#                   MemVSDOFonLaptop.py

# Author: Michael Weiss, March 2024

# 0) Run mesh_example_2.py first (python mesh_example_2.py)
# 1) Make *.sh executable using chmod +x *.sh in terminal
# 2) Adjust "username" when calling grep in run.sh

# Run three simulations with various grids
# Important: make sure meshes=['null'] in run_example_2.py
# Note: Simulations are run with TWO MPI PROCESSES


sed -i 's/null/example_2_mesh_coarse/g' run_example_2.py

./run.sh

cp memtrack.txt memtrack_it_coarse.txt

rm memtrack.txt memtrack.log

sed -i 's/example_2_mesh_coarse/example_2_mesh_inter/g' run_example_2.py

./run.sh

cp memtrack.txt memtrack_it_inter.txt

rm memtrack.txt memtrack.log

sed -i 's/example_2_mesh_inter/example_2_mesh_fine/g' run_example_2.py

./run.sh

cp memtrack.txt memtrack_it_fine.txt

rm memtrack.txt memtrack.log

python TimeVsDOFonLaptop.py

./PlotMemory.sh



