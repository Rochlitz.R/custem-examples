# Prepare txt file for python

# PRESB
# Coarse
#sed 's/\,/./g' memtrack_coarse_direct.txt  >> new.txt
#sed 's/g/ /g'  new.txt >> readyforread.txt
sed 's/g/ /g'  memtrack_it_coarse.txt >> readyforread.txt

# Intermediate
sed 's/g/ /g' memtrack_it_inter.txt >> readyforread1.txt

# Fine
sed 's/g/ /g' memtrack_it_fine.txt >> readyforread2.txt

python MemVsDOFonLaptop.py

rm readyforread.txt readyforread1.txt readyforread2.txt
