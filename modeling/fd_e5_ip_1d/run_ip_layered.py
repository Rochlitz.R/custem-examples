# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 1                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
import numpy as np
import dolfin as df


freqs = np.logspace(0, 4, 5)         # check for multiple freqs later on
mesh = 'ip_3l'
p = 2

# Initialize MODel
mod = 'p' + str(p) + '_new' #+ '_no_ip'
M = MOD(mod, mesh, 'E_t', p=p, overwrite_results=True,
        overwrite_mesh=True, serial_ordering=True,
        m_dir='./meshes', r_dir='./results')

# # define frequency and conductivities and ip parameters
M.MP.update_model_parameters(frequencies=freqs,
                             ip_c=[0., 0.8, 1., 0.],
                             ip_m=[0., 0.7, 0.5, 0.],
                             ip_tau=[0., 1., 0.1, 0.],
                             sigma_ground=[1e-2, 1e-1, 1e-3],
                             sigma_0=[1e-2, 1e-2, 1e-2])

# set up the variational formulations
M.FE.build_var_form(ip=True)

# call solver and convert H-fields
M.solve_main_problem(auto_interpolate=True)

# interpolate results on observation line, alternative to auto_interpolate
M.IB.create_line_mesh('x', 0., 200., 20, y=100., z=-0.1, line_name='obs')
M.IB.interpolate('obs_line_x')





# # raphas stuff - ignore  # #
#     bbt = M.FS.mesh.bounding_box_tree()

#     val = df.Point(rx[0, 0], rx[0, 1], rx[0, 2])
#     E_t_r, E_t_i = M.FS.U[0].split(True)

#     if bbt.compute_first_entity_collision(val) < M.FS.mesh.num_cells():
#         print(E_t_r(val))
#         Er = E_t_r(val)
#         Ei = E_t_i(val)
#     else:
#         Er = None
#         Ei = None

# #    val = df.Point(rx[1, 0], rx[1, 1], rx[1, 2])
# #    E_t_r, E_t_i = M.FS.U[0].split(True)
# #
# #    if bbt.compute_first_entity_collision(val) < M.FS.mesh.num_cells():
# #        print(E_t_r(val))
# #        E_r2.append(E_t_r(val))
# #        E_i2.append(E_t_i(val))
# #    else:
# #        pass
# #
# #    val = df.Point(rx[2, 0], rx[2, 1], rx[2, 2])
# #    E_t_r, E_t_i = M.FS.U[0].split(True)
# #
# #    if bbt.compute_first_entity_collision(val) < M.FS.mesh.num_cells():
# #        print(E_t_r(val))
# #        E_r3.append(E_t_r(val))
# #        E_i3.append(E_t_i(val))
# #    else:
# #        pass

#     if Er is not None:
#         np.save('E_r' + str(jj) + '_noip.npy', np.array(Er))
#         np.save('E_i' + str(jj) + '_noip.npy', np.array(Ei))
#     else:
#         print(df.MPI.rank(M.MP.mpi_cw))

#if len(E_r2) != 0:
#    np.save('E_r2_ip.npy', np.array(E_r2))
#    np.save('E_i2_ip.npy', np.array(E_i2))
#
#if len(E_r3) != 0:
#    np.save('E_r3_ip.npy', np.array(E_r3))
#    np.save('E_i3_ip.npy', np.array(E_i3))