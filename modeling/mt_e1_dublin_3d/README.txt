﻿################################################################################
# # # # #                  magnetotelluric Example 1                   # # # # #
################################################################################

  magnetoelluric modeling of Dublin test model compared to external references
  
  The available reference data in the *refdata* directory need to be extracted.
  They were downloaded on December 7, 2020 from
  https://www.mtnet.info/workshops/mt3dinv/2008_Dublin/dublin_forward.html
  
################################################################################

    - Start with generating the mesh by running the *mesh_mt_dublin.py* file:

        > python mesh_mt_dublin.py
    
    - The *run_mt_dublin.py* script should be called in parallel:

        > mpirun -n 24 python -u run_mt_dublin.py (and export OMP_NUM_THERADS=2,
                                                   recommended for p2)
    
    - The computation will requie less than 64 GB RAM with this 
      parallelization configuration.  
    
    - The *plot_mt_dublin.py* script can be used to compare the different 
	  reference data from www.mtnet.info with the custEM results

        > python plot_mt_dublin.py 

################################################################################