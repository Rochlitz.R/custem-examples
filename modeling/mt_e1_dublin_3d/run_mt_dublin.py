# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
from custEM.misc import mpi_print as mpp
import numpy as np

# specify freqs, mesh and polynomial order
periods = [0.1, 0.18, 0.32, 0.56, 1., 1.8, 3.2, 5.6,
           10., 18., 32., 56., 100., 180., 320., 560.,
           1000., 1800., 3200., 5600., 10000.]
freqs = 1. / np.array(periods)

p = 1
mesh = 'dublin'


# Initialize MODel
mod = 'p' + str(p)

M = MOD(mod, mesh, 'MT', p=p, overwrite_results=True,
        overwrite_mesh=True, serial_ordering=True, #mumps_debug=True,
        m_dir='./meshes', r_dir='./results')

# define frequency and condcutivities
M.MP.update_model_parameters(frequencies=freqs,
                             sigma_ground=[1e-2, 0.1, 1., 1e-4])

# build var form and solve SLE
M.FE.build_var_form()

M.solve_main_problem(export_pvd=False,
                     export_nedelec=False,
                     auto_interpolate=True)

M.IB.convert_mt_data(ned_coord_system=True, derivatives=True)
