﻿################################################################################
# # # # #                    time-domain Example 2                     # # # # #
################################################################################

      comparison of all time-domain approaches against SLDMEM, LOTEM setup

################################################################################

    - Start with generating the mesh by running the *mesh_td_example_2.py* file:

        > python mesh_td_example_2.py
    
    - The *run_td_example_2.py* script should be called in parallel:

        > mpirun -n 12 python -u run_example_1.py (and export OMP_NUM_THERADS=2,
                                                   recommended for p2)
    
    - The computation will requie less than 64 GB RAM with this 
      parallelization configuration.  
    
    - The *plot_td_example_2_***.py* scripts will generate identical plots as
      shown in the corresponding publication (Rochlitz et al., 2021) in
      the *plots* directory.

        > python plot_td_example_2_dB.py  (or *E* instead of *dB*)

################################################################################