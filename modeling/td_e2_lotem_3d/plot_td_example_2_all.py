# -*- coding: utf-8 -*-
"""
Created on Tue Dec 09 16:06:00 2014

@author: moouuzi
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams as rc
from scipy.integrate import cumtrapz
from matplotlib.colors import LogNorm
from matplotlib import gridspec
from scipy.interpolate import interp1d


def resample(t_in, field_in, t_out):

    func = interp1d(t_in, field_in, kind='cubic')
    return(func(t_out))


prop_cycle = plt.rcParams['axes.prop_cycle']
clr = prop_cycle.by_key()['color']


label_color = '#002C72'
# rc['axes.edgecolor'] = label_color
# rc['xtick.color'] = label_color
# rc['ytick.color'] = label_color
# rc['axes.labelcolor'] = label_color
# rc['text.color'] = label_color
rc['axes.unicode_minus'] = False


# %% plot B

path0 = 'results/E_IE/e2_lotem/'
path1 = 'results/E_FT/e2_lotem/'
path2 = 'results/E_RA/e2_lotem/'
name = 'p2'

pos = ['x-1000_y500',
       'x-1000_y1000',
       'x-1000_y1500',
       'x-1000_y2000',
       'x-1000_y2500',
       'x-1000_y3000',
       'x0_y500',
       'x0_y1000',
       'x0_y1500',
       'x0_y2000',
       'x0_y2500',
       'x0_y3000',
       'x+1000_y500',
       'x+1000_y1000',
       'x+1000_y1500',
       'x+1000_y2000',
       'x+1000_y2500',
       'x+1000_y3000']


fem_times = np.logspace(-4, 0, 41)
all_errs1, all_errs2, all_errs3 = [], [], []
all_errs4, all_errs5, all_errs6 = [], [], []

for kk, stn in enumerate(range(18)):

    E_field0, E_field1, E_field2 = [], [], []
    B_field0, B_field1, B_field2 = [], [], []

    E_field0 = np.load(path0 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y1 = np.linalg.norm(E_field0, axis=1)

    E_field1 = np.load(path1 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y2 = np.linalg.norm(E_field1, axis=1)

    E_field2 = np.load(path2 + name + '_interpolated/E_shut_off_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y3 = np.linalg.norm(E_field2, axis=1)

    B_field0 = np.load(path0 + name + '_interpolated/dH_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y4 = np.linalg.norm(B_field0, axis=1) * (4.* np.pi * 1e-7) * 1e9

    B_field1 = np.load(path1 + name + '_interpolated/dH_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y5 = np.linalg.norm(B_field1, axis=1) * (4.* np.pi * 1e-7) * 1e9

    B_field2 = np.load(path2 + name + '_interpolated/dH_' +
                       'e2_rec_path.npy')[stn, :3, :].T
    y6 = np.linalg.norm(B_field2, axis=1) * (4.* np.pi * 1e-7) * 1e9

    sldmem = np.abs(np.loadtxt('sldmem/e_' + pos[kk] + '_sldmem.logfile', skiprows=1)[:, 1:])
    ref = np.linalg.norm(sldmem, axis=1) * 1e-6  # sldmem data in mV/m

    sldmem2 = np.abs(np.loadtxt('sldmem/b_' + pos[kk] + '_sldmem.logfile', skiprows=1)[:, 1:])
    ref2 = np.linalg.norm(sldmem2, axis=1)

    prms1 = np.abs((y1 - ref)/(ref + y1)) * 200.
    prms2 = np.abs((y2 - ref)/(ref + y2)) * 200.
    prms3 = np.abs((y3 - ref)/(ref + y3)) * 200.

    prms4 = np.abs((y4 - ref2)/(ref2 + y4)) * 200.
    prms5 = np.abs((y5 - ref2)/(ref2 + y5)) * 200.
    prms6 = np.abs((y6 - ref2)/(ref2 + y6)) * 200.

    all_errs1.append(np.median(np.abs(prms1)))
    all_errs2.append(np.median(np.abs(prms2)))
    all_errs3.append(np.median(np.abs(prms3)))

    all_errs4.append(np.median(np.abs(prms4)))
    all_errs5.append(np.median(np.abs(prms5)))
    all_errs6.append(np.median(np.abs(prms6)))

    # %% plotting part

M, N = np.meshgrid(np.arange(4), np.arange(7))
arr1 = np.array(all_errs1).reshape(3, 6)
arr2 = np.array(all_errs2).reshape(3, 6)
arr3 = np.array(all_errs3).reshape(3, 6)

arr4 = np.array(all_errs4).reshape(3, 6)
arr5 = np.array(all_errs5).reshape(3, 6)
arr6 = np.array(all_errs6).reshape(3, 6)

cmap1 = plt.get_cmap('coolwarm', 20)
cmap1.set_bad(color = '0.6', alpha = 1.)
cmap1.set_under(color = '1.0', alpha = 1.)
cmap1.set_over(color = '0.0', alpha = 1.)

fig = plt.figure(figsize=(12,4))
gs = gridspec.GridSpec(2,3)
fs = 10

ax1=fig.add_subplot(gs[0, 0], aspect="equal",adjustable='box')
ax2=fig.add_subplot(gs[0, 1], aspect="equal",adjustable='box')
ax3=fig.add_subplot(gs[0, 2], aspect="equal",adjustable='box')
ax4=fig.add_subplot(gs[1, 0], aspect="equal",adjustable='box')
ax5=fig.add_subplot(gs[1, 1], aspect="equal",adjustable='box')
ax6=fig.add_subplot(gs[1, 2], aspect="equal",adjustable='box')

ax1.pcolor(arr1, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))
ax2.pcolor(arr2, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))
ax3.pcolor(arr3, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))

ax1.set_title(r'$med(\epsilon_{||\mathbf{e}||})$ IE - SLDMEM', fontsize=fs)
ax2.set_title(r'$med(\epsilon_{||\mathbf{e}||})$ FT - SLDMEM', fontsize=fs)
ax3.set_title(r'$med(\epsilon_{||\mathbf{e}||})$ RA - SLDMEM', fontsize=fs)

ax4.pcolor(arr4, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))
ax5.pcolor(arr5, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))
obj = ax6.pcolor(arr6, cmap=cmap1, norm=LogNorm(vmin=1e-1, vmax=1e1))

ax4.set_title(r'$med(\epsilon_{||\partial\mathbf{b}/\partial t||})$ IE - SLDMEM', fontsize=fs)
ax5.set_title(r'$med(\epsilon_{||\partial\mathbf{b}/\partial t||})$ FT - SLDMEM', fontsize=fs)
ax6.set_title(r'$med(\epsilon_{||\partial\mathbf{b}/\partial t||})$ RA - SLDMEM', fontsize=fs)

for ax in [ax1,ax2,ax3,ax4,ax5,ax6]:
    ax.set_xticks(np.arange(6) + 0.5)
    ax.set_yticks(np.arange(3) + 0.5)

for ax in [ax4,ax5,ax6]:
    ax.set_xticklabels(['0.5', '1', '1.5', '2', '2.5', '3'])
    ax.set_xlabel('x (km)')

for ax in [ax1, ax4]:
    ax.set_yticklabels(['-1', '0', '1'])
    ax.set_ylabel('y (km)')

plt.colorbar(obj, ax=[ax1,ax2,ax3,ax4,ax5,ax6],
             label=r'$\epsilon$ (%)', orientation='vertical')

plt.savefig('plots/all_misfits.pdf', dpi=300, bbox_inches='tight')
plt.show()