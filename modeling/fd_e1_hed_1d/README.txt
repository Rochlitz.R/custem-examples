﻿################################################################################
# # # # #                  frequency-domain Example 1                  # # # # #
################################################################################

      homogeneous halfspace model with HED and broadside observation line

################################################################################

    - Start with generating the mesh by running the *mesh_example_1.py* script:

        > python mesh_example_1.py
    
    - If for some reason TetGen could not be installed, the required mesh can
      be made available by extracting the zip-file in this directory. 

    - The *run_example_1.py* script can be called either in serial (also use
      mpirun syntax) or in parallel.

        > mpirun -n 1 python run_example_1.py
        > mpirun -n 4 python run_example_1.py
    
    - Please note that the computation with calling the *run_example_1.py*
      script requires ~8 GB RAM, if 4 mpi-processes
      are used. With 2 mpi_processes or in serial, it will be definitely <8 GB! 
    
    - The *plot_example_1.py* script will generate the identical plot as shown
      in Figure 3 in the corresponding publication (Rochlitz et al., 2019) in
      the *plots* directory, named "example_1.pdf".

        > python plot_example_1.py

################################################################################