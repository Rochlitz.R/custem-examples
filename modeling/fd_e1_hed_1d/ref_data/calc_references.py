# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 16:24:11 2018

@author: Skibbe.N
"""
from custEM.wrap import pyhed as ph
from custEM.wrap.pyhed.hed.reference import makeRunFileFromLoop
import numpy as np


def calcEmpymod_brute(Lp, add='', z_offset=0.01):
    from empymod import bipole
    Ftype = Lp.config.ftype.upper() in ['H', 'B']
    srces = np.array((Lp.pos[:, 0], Lp.pos[:, 1], Lp.pos[:, 2] + z_offset,
                      np.rad2deg(Lp.phi), np.zeros_like(Lp.phi)))
    depth = [0]  # start of layer1, start of layer2, ...
    depth.extend(np.cumsum(Lp.config.d).tolist())
    res = [1e20]  # air, layer1, layer2, ...
    res.extend(list(Lp.config.rho))
    freq = Lp.config.f
    receiver = Lp.loopmesh.positions().array().T
    LoopField = np.zeros((3, Lp.loopmesh.nodeCount()), complex)
    opt = 'spline'
    for scr_i in range(len(srces[0])):
        # print('dipole', scr_i)
        src = list(srces[:, scr_i])
        All = np.zeros_like(LoopField)
        for i in range(len(receiver[0])):
            rx = receiver[:, i]
            recx = [rx[0], rx[1], -rx[2], 0, 0]  # x
            All[0, i] = bipole(src, recx, depth, res, freq,
                               verb=0, mrec=Ftype, opt=opt)
            recy = [rx[0], rx[1], -rx[2], 90, 0]  # y
            All[1, i] = bipole(src, recy, depth, res, freq,
                               verb=0, mrec=Ftype, opt=opt)
            recz = [rx[0], rx[1], -rx[2], 0, -90]  # -z
            # -z to match the right_handed coordinate system
            All[2, i] = bipole(src, recz, depth, res, freq,
                               verb=0, mrec=Ftype, opt=opt)
            # raise SystemExit
        LoopField -= All * Lp.ds[scr_i]  # coordinate convention
    Lp.field = LoopField
#    Lp.exportVTK('{}_{}_{}_empymod.vtk'.format(add, 'tetm', Lp.config.ftype),
#                 components=True)
    return Lp


if __name__ == '__main__':
    l = ph.loop.buildDipole([0, 0, 0], angle=np.pi/2.)
    freq = [1., 10., 100., 1000.]
    l.setModel([100.])

    coords = np.zeros((3, 2001))
    coords[0] = np.linspace(-5000., 5000., 2001)

    l.setLoopMesh(coords)
    l.loopmesh.save('line4freq.bms')

    for f in freq:
        l.config.f = f
        # dipole 1d runfile
        makeRunFileFromLoop(l, filename='f_{:.0f}'.format(f),
                            result_filename='f_{:.0f}.csem'.format(f))

        # pyhed
        l.config.ftype = 'H'
        l.config.mode = 'tetm'
        print(l.config)
        l.calculate()
        np.save('H_pyhed_freq_{:.0f}.npy'.format(l.config.f), l.field)
        l.field = None

        # empymod
        l.config.ftype = 'B'
        l.config.mode = 'tetm'
        print('############################################################')
        print(l.config)
        l = calcEmpymod_brute(l)
        np.save('E_empymod_freq_{:.0f}.npy'.format(l.config.f), l.field)

# The End
