# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 1                          # # # # #
# ########################################################################### #
# # # # #                     visualization script                    # # # # #
# ########################################################################### #

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from custEM.post import PlotFD as Plot
from custEM.post import make_plain_subfigure_box

# define matplotlib parameters
plt.ioff()
rcParams['axes.unicode_minus'] = False
rcParams['figure.figsize'] = 8, 11

# define further utility parameters for plotting
clrs = ['#00539f', '#c54a11', '#09ceff', '#ffb90f']
lws = 0.5
llabels = ['1 Hz', '10 Hz', '100 Hz', '1000 Hz']
freqs_str = ['1', '10', '100', '1000']
linE = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_1000_line_x'
mesh = 'example_1_mesh'
mod = 'example_1_1'
fs = 10
data_lim = [1e-15, 1e-3]
ri_list = ['real', 'imag']
ri_label = ['\Re', '\Im']

# inititalize figure and Plot class
fig, ax = make_plain_subfigure_box(3, 2, fs=fs, log_scale=False)
P = Plot(mod=mod, mesh=mesh, approach='E_t',
         r_dir='./results', s_dir='./plots')

# plot everything for all 4 frequencies
for j, freq in enumerate(freqs_str):
    P.rel_comp_errors = dict()
    mod = 'p1'
    t1 = 'Tp1' + str(j)
    s1 = 'Sp1' + str(j)

    P.import_line_data(linE, mod=mod, mesh=mesh, approach='E_t',
                       freq=j, key=t1)
    P.import_line_data(linE, mod=mod, mesh=mesh, approach='E_s',
                       freq=j, key=s1, sf=False)
    P.line_data['empymod_ref' + freq + '_E_t'] = np.load(
            './ref_data/E_empymod_freq_' + freq + '.npy').T[::2]
    P.line_data['empymod_ref' + freq + '_H_t'] = np.load(
            './ref_data/H_empymod_freq_' + freq + '.npy').T[::2]

    P.line_data['dipole1d_ref' + freq + '_E_t'] = np.load(
            './ref_data/E_dipole1d_freq_' + freq + '.npy').T[::2]
    P.line_data['dipole1d_ref' + freq + '_H_t'] = np.load(
            './ref_data/H_dipole1d_freq_' + freq + '.npy').T[::2]

    P.line_data['dipole1d_ref' + freq + '_H_t'] /= 4e-7 * np.pi

    for flg in ['_E_t', '_H_t']:
        P.abs_errors(P.line_data[s1 + flg],
                     P.line_data['empymod_ref' + freq + flg])

    # plot all data for real and imag in the order 'E_t', pyhed, 'empymod'
    for jj, ri in enumerate(ri_list):
        P.add_to_line_plot(
                ax, [0, jj], comp='x', ri=ri_list[jj], key=t1, EH='H',
                ls='-', color='0.2', lw=lws, tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_x$)')
        P.add_to_line_plot(
                ax, [0, jj], comp='x', ri=ri_list[jj], key='dipole1d_ref' +
                freq, EH='H', color=clrs[j], lw=lws, ls='-.', tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_x$)')
        P.add_to_line_plot(
                ax, [0, jj], comp='x', ri=ri_list[jj], key=s1, EH='H',
                ls='--', lw=lws, color=clrs[j], tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_x$)')
        P.add_to_line_plot(
                ax, [0, jj], comp='x', ri=ri_list[jj], EH='H', lw=lws,
                color=clrs[j], ls=':', key='empymod_ref' + freq, tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_x$)')
        P.add_to_line_plot(
                ax, [1, jj], comp='y', ri=ri_list[jj], key=t1, EH='E',
                ls='-', lw=lws, color=clrs[j], tcolor='k', llabel=llabels[j],
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')

        if j < 3:
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key=t1, EH='E',
                    ls='-', lw=lws, color='0.2', tcolor='k',
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key='dipole1d_ref' +
                    freq, EH='E', color=clrs[j], lw=lws, ls='-.',
                    fs=fs, tcolor='k',
                    label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key=s1, EH='E',
                    ls='--', lw=lws, color=clrs[j], tcolor='k',
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], EH='E', tcolor='k',
                    color=clrs[j], ls=':', key='empymod_ref' + freq, lw=lws,
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
        else:
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key=t1, EH='E',
                    tcolor='k',
                    ls='-', lw=lws, color='0.2', llabel='FE: TF $E$',
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key='dipole1d_ref' +
                    freq, EH='E', color=clrs[j], lw=lws, ls='-.',
                    fs=fs, llabel='dipole1d', tcolor='k',
                    label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], key=s1, EH='E',
                    tcolor='k',
                    ls='--', lw=lws, color=clrs[j], llabel='pyhed',
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')
            P.add_to_line_plot(
                    ax, [1, jj], comp='y', ri=ri_list[jj], EH='E', ls=':',
                    tcolor='k', lw=lws,
                    color=clrs[j], key='empymod_ref' + freq, llabel='empymod',
                    fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{E}_y$)')

        P.add_to_line_plot(
                ax, [2, jj], comp='z', ri=ri_list[jj], key=t1, EH='H', ls='-',
                color='0.2', lw=lws, tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_z$)')
        P.add_to_line_plot(
                ax, [2, jj], comp='z', ri=ri_list[jj], key='dipole1d_ref' +
                freq, EH='H', color=clrs[j], lw=lws, ls='-.', tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_z$)')
        P.add_to_line_plot(
                ax, [2, jj], comp='z', ri=ri_list[jj], key=s1, EH='H',
                ls='--', lw=lws, color=clrs[j], tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_z$)')
        P.add_to_line_plot(
                ax, [2, jj], comp='z', ri=ri_list[jj], EH='H', lw=lws,
                color=clrs[j], ls=':', key='empymod_ref' + freq, tcolor='k',
                fs=fs, label=r'$' + ri_label[jj] + '$($\mathbf{H}_z$)')

# make legend and adjust axes limits and apply log_scale to y-axis
ax[-2, -1].legend(loc=1)
for j in range(2):
    for k in range(2):
        ax[j, k].set_yscale('log')
        ax[j, k].set_ylabel('E [V/m]')
        ax[j, k].set_xlim([-5., 5.])
        ax[j, k].set_ylim(data_lim)

for k in range(2):
    ax[2, k].set_yscale('log')
    ax[2, k].set_ylabel('H [A/m]')
    ax[2, k].set_xlabel('x [km]')
    ax[2, k].set_xlim([-5., 5.])
    ax[2, k].set_ylim(data_lim)

# save figure
plt.savefig('plots/example_1.pdf', bbox_inches='tight', pad_inches=0)
# plt.ion()   # uncomment to enable showing the plot
# plt.show()  # uncomment to enable showing the plot
