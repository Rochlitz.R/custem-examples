# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 1                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD
import numpy as np

approaches = ['E_t', 'E_s']    # E_s computation == pyhed reference
                               # if no anomalies are incorporated in a mesh
freqs = [1e0, 1e1, 1e2, 1e3]   # specify computation frequencies

# ########################## run p1 computations ############################ #

p = 1
mesh = 'example_1_mesh'

a = np.zeros((10, 3))
a[:, 1] = np.arange(10)

for approach in approaches:       # two approachesa
    # Initialize MODel
    mod = 'p1'
    M = MOD(mod, mesh, approach, p=p, overwrite_results=True,
            m_dir='./meshes', r_dir='./results', debug_level=10)

    # define frequency and condcutivities
    M.MP.update_model_parameters(frequencies=freqs,
                                 sigma_ground=[1e-2],
                                 sigma_0=[1e-2])
    # build the variational formulation
    M.FE.build_var_form(s_type='hed',  # in this example 1 Tx
                        azimuth=90.)   # specified manually

    # call solver and convert H-fields
    M.solve_main_problem(export_pvd=False)

    M.IB.create_line_mesh('x', -5e3, 5e3, 1000, force_create=True)
    linE = 'x0_-5000.0_x1_5000.0_y_0.0_z_0.0_n_1000_line_x'  # default name

    # interpolate fields on the observation line
    M.IB.interpolate(linE)