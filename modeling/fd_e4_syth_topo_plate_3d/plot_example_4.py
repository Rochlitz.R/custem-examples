# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 4                          # # # # #
# ########################################################################### #
# # # # #                     visualization script                    # # # # #
# ########################################################################### #

from custEM.post import PlotFD as Plot
import matplotlib.pyplot as plt
from matplotlib import rcParams

# set matplotlib parameters
plt.ioff()
rcParams['axes.unicode_minus'] = False

# define and import calculated FEM solutions interpolated on the surface (topo)
# and a slice with 50m constant offset to the surface
slicE_0, slicE_50 = 'surface_slice_z', 'surface_50m_above_slice_z'
mesh1 = 'example_4_mesh'
mod1 = 'p2'
P = Plot(mod=mod1, mesh=mesh1, approach='E_t',
         r_dir='./results', s_dir='./plots')
k1, k2, k3, k4 = 'M1', 'M2', 'M3', 'M4'
P.import_slice_data(slicE_0, mod=mod1, key=k1, EH='E', stride=4)
P.import_slice_data(slicE_50, mod=mod1, key=k2, EH='H', stride=4)
P.import_slice_data(slicE_0, mod=mod1, key=k3, EH='E', stride=4, coord_key=k3)
P.import_slice_data(slicE_50, mod=mod1, key=k4, EH='H', stride=4, coord_key=k4)

# plot 3D data
fig = plt.figure(figsize=(10, 7))
P.add_3D_slice_plot(fig, 'M1_E_t', slicE_0, sp_pos=111, q_length=0.2,
                    c_lim=[1e-8, 1e-6], label=r'$\Re(\mathbf{|E|})$ $(V/ m)$',
                    q_slicE=k3, q_name=k3 + '_E_t')
plt.savefig('./plots/example_4_E_r.pdf')

fig = plt.figure(figsize=(10, 7))
P.add_3D_slice_plot(fig, 'M1_E_t', slicE_0, sp_pos=111, q_length=0.2,
                    c_lim=[1e-7, 1e-4], label=r'$\Im(\mathbf{|E|})$ $(V/ m)$',
                    q_slicE=k3, q_name=k3 + '_E_t', ri='imag')
plt.savefig('./plots/example_4_E_i.pdf')

fig = plt.figure(figsize=(10, 7))
P.add_3D_slice_plot(fig, 'M2_H_t', slicE_50, sp_pos=111, q_length=0.3,
                    c_lim=[1e-5, 1e-2], label=r'$\Re(\mathbf{|H|})$ $(A/ m)$',
                    q_slicE=k4, q_name=k4 + '_H_t', cmap='viridis')
plt.savefig('./plots/example_4_H_r.pdf')

fig = plt.figure(figsize=(10, 7))
P.add_3D_slice_plot(fig, 'M2_H_t', slicE_50, sp_pos=111, q_length=0.3,
                    c_lim=[1e-7, 1e-5], label=r'$\Im(\mathbf{|H|})$ $(A/ m)$',
                    q_slicE=k4, q_name=k4 + '_H_t', ri='imag',  cmap='viridis')
plt.savefig('./plots/example_4_H_i.pdf')

# plt.ion()   # uncomment to enable showing the plots
# plt.show()  # uncomment to enable showing the plots
