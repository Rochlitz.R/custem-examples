﻿################################################################################
# # # # #                  frequency-domain Example 6                  # # # # #
################################################################################

                homogeneous halfspace model with infrstructure

################################################################################

    - Use either Prism-like volumetric representation of infrastructure with 
      high conductivities or inner Zero-Dirichlet conditions to represent
      infrastructure in a CSEM model. 
    - This example uses a vertical borehole and airborne receivers. It is just
      a first model that should be modified to see significant effects of the
      infrastructure on the EM fields.

################################################################################