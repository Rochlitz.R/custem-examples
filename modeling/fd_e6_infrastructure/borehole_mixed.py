# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 6                          # # # # #
# ########################################################################### #

import numpy as np
import dolfin as df

from custEM.core import MOD
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.meshgen_utils import line_x, line_y, line_z
from custEM.meshgen.meshgen_utils import rx_grid, refine_rx
import custEM.meshgen.meshgen_utils as mu


# %% generate mesh
if df.MPI.rank(df.MPI.comm_world) == 0:


    # define outcrop of prisma at surface, these points are added in the 2D
    # surface mesh during *build_surface* and connected with the uppermost prism
    # later on automatically

    bx = 200.
    by = 1000.
    r = 0.05
    h = 0.1

    outcrop = mu.loop_c(r=r, n_segs=3, z=0.)
    outcrop[:, 0] += bx
    outcrop[:, 1] += by


    # create world
    M = BlankWorld(name='borehole_mesh_mixed', preserve_edges=True)

    # define airborne Rx
    rx_air = rx_grid([-500., 500., 21], [500., 1500., 21], 50.)
    rx_tri = refine_rx(rx_air, 5., 30.)

    # add observation line: 10 km x-directed observation line
    M.build_surface(insert_line_tx=[line_x(start=-500., stop=500., n_segs=101)],
                    insert_points=outcrop,
                    insert_paths=rx_tri)

    # build halfspace mesh and extend 2D surface mesh to 3D world
    M.build_halfspace_mesh()
    rx_air[:, 2] = -1.
    M.add_rx(rx_air)
    #M.add_paths(rx_tri)

    # add edges representing the infrastructure to the mesh
    M.add_paths([line_z(-500., -301., x=bx, y=by, n_segs=199)])

    # discretize small elongated structure by many small prisms to ensure
    # good mesh quality. Triangular prisms can be perfectly discretized
    # with 2 tetrahedra.
    for d in np.linspace(-0.05, -300.05, 3001):
        M.add_cylinder(r, h, [bx, by, d], n_segs=3)

    # build a pyramid by adding three additional triangles at the end of the
    # prism to connect to the edge-representation
    ids = mu.get_closest([bx, by, -301.], M.Omega.positions().array(), n=4)
    for i, j, k in [[0, 1, 2], [0, 2, 3], [0, 1, 3]]:
        M.Omega.createTriangleFace(M.Omega.node(ids[i]),
                                    M.Omega.node(ids[j]),
                                    M.Omega.node(ids[k]))

    # call TetGen
    M.remove_duplicate_poly_faces()
    M.call_tetgen(tet_param='-pq1.3aAT1e-10', print_infos=False)
else:
    pass

df.MPI.barrier(df.MPI.comm_world)


# %% run simulation

# specify computation frequencies
freqs = [1e0, 1e1, 1e2, 1e3]

p = 1
mesh = 'borehole_mesh_mixed'
mod = 'p' + str(p) + '_Mixed'

# initialize model
M = MOD(mod, mesh, 'E_t', p=p, overwrite_mesh=True,
        overwrite_results=True, serial_ordering=True, debug_level=10)

# sigmaground = halfspace + 1000 borehole segments with 1 m length
# define frequency and condcutivities
M.MP.update_model_parameters(frequencies=freqs,
                             sigma_ground=[1e-3] + [1e3] * 3002)

M.FS.bc_geometry = 'x[0] > 199.99 && x[0] < 200.01 && x[1] > 999.99 && ' +\
                   'x[1] < 1000.01 && x[2] > -500.01 && x[2] < -300.99'

# build the variational formulation
M.FE.build_var_form(bc='IF')

# call solver and convert H-fields
M.solve_main_problem(export_pvd=True)

#%% visualization

# create interopolation mesh for airborne receivers
M.IB.create_slice_mesh('z', 1e3, 20, y=1000., z=50., slice_name='rxair')

# interpolate fields on the observation line
# for lines or slices along axes, aways *line_x/y/z* or *slice_x/y/z* are
# appended to the name
M.IB.interpolate(['rxair_slice_z'])