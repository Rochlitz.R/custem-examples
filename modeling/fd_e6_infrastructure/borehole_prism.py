# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 6                          # # # # #
# ########################################################################### #

import numpy as np
import dolfin as df

from custEM.core import MOD
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.meshgen_utils import line_x, line_y, line_z
from custEM.meshgen.meshgen_utils import rx_grid, refine_rx

# %% generate mesh
if df.MPI.rank(df.MPI.comm_world) == 0:
    # create world
    M = BlankWorld(name='borehole_mesh_prism', preserve_edges=True)

    # define airborne Rx
    rx_air = rx_grid([-500., 500., 21], [500., 1500., 21], 50.)
    rx_tri = refine_rx(rx_air, 5., 30.)

    # add observation line: 10 km x-directed observation line
    M.build_surface(insert_line_tx=[line_x(start=-500., stop=500., n_segs=21)])

    # build halfspace mesh and extend 2D surface mesh to 3D world
    M.build_halfspace_mesh()

    # add receivers
    M.add_rx(rx_air)
    M.add_paths(rx_tri)

    # discretize small elongated structure by many small prisms to ensure
    # good mesh quality. Triangular prisms can be perfectly discretized
    # with 2 tetrahedra.
    for d in np.linspace(-1., -1000., 1000):
        M.add_cylinder(1., 1., [200., 1000., d], n_segs=3)

    # call TetGen
    M.remove_duplicate_poly_faces()
    M.call_tetgen(tet_param='-pq1.3aA')
else:
    pass

df.MPI.barrier(df.MPI.comm_world)

# %% run simulation

# specify computation frequencies
freqs = [1e0, 1e1, 1e2, 1e3]

p = 2
mesh = 'borehole_mesh_prism'
mod = 'p' + str(p) + '_Prism'

# initialize model
M = MOD(mod, mesh, 'E_t', p=p, overwrite_mesh=True,
        overwrite_results=True, serial_ordering=True, debug_level=10)

# sigmaground = halfspace + 1000 borehole segments with 1 m length
# define frequency and condcutivities
M.MP.update_model_parameters(frequencies=freqs,
                             sigma_ground=[1e-3] + [1e3] * 1000)

# build the variational formulation
M.FE.build_var_form()

# call solver and convert H-fields
M.solve_main_problem(export_pvd=True)

#%% visualization

# create interopolation mesh for airborne receivers
M.IB.create_slice_mesh('z', 1e3, 20, y=1000., z=50., slice_name='rxair')

# interpolate fields on the observation line
# for lines or slices along axes, aways *line_x/y/z* or *slice_x/y/z* are
# appended to the name
M.IB.interpolate(['rxair_slice_z'])