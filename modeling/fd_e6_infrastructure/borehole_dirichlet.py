# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 6                          # # # # #
# ########################################################################### #

import numpy as np
import dolfin as df

from custEM.core import MOD
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.meshgen_utils import line_x, line_y, line_z
from custEM.meshgen.meshgen_utils import rx_grid, refine_rx


# %% generate mesh
if df.MPI.rank(df.MPI.comm_world) == 0:
    # create world
    M = BlankWorld(name='borehole_mesh_dirichlet', preserve_edges=True)

    # define airborne Rx
    rx_air = rx_grid([-500., 500., 21], [500., 1500., 21], 50.)
    rx_tri = refine_rx(rx_air, 5., 30.)

    # add observation line: 10 km x-directed observation line
    M.build_surface(insert_line_tx=[line_x(start=-500., stop=500., n_segs=21)])

    # build halfspace mesh and extend 2D surface mesh to 3D world
    M.build_halfspace_mesh()

    # add receivers
    M.add_rx(rx_air)
    M.add_paths(rx_tri)

    # add edges representing the infrastructure to the mesh
    M.add_paths([line_z(-1000., -1., y=1000., x=200., n_segs=100)])

    # call TetGen
    M.remove_duplicate_poly_faces()
    M.call_tetgen(tet_param='-pq1.3aA')
else:
    pass

df.MPI.barrier(df.MPI.comm_world)

# %% run simulation

# specify computation frequencies
freqs = [1e0, 1e1, 1e2, 1e3]

p = 2
mesh = 'borehole_mesh_dirichlet'
mod = 'p' + str(p) + '_IF'

# define model and choose debug level 10 to show the detected dof-coordinates
# of the inner Dirichlet-Conditions for debugging in the beginning, level 20
# or higher will not print such details
M = MOD(mod, mesh, 'E_t', p=p, overwrite_mesh=True,
        overwrite_results=True, serial_ordering=True, debug_level=10)

# define geometry for searching dof belonging to the borehole, maybe it is
# possible to find an easier alternative here with other FEniCS methods or
# it could be possible to find an automatism for automatically generating these
# string representations for arbitrary line segments in future
M.FS.bc_geometry = 'x[0] > 199.99 && x[0] < 200.01 && x[1] > 999.99 && ' +\
                   'x[1] < 1000.01 && x[2] > -2000.01 && x[2] < 0.99'

# define frequency and condcutivities
M.MP.update_model_parameters(frequencies=freqs,
                             sigma_ground=[1e-3],
                             current=-1.)

# build the variational formulation
M.FE.build_var_form(bc='IF')

# call solver and convert H-fields
M.solve_main_problem(export_pvd=True)

#%% visualization

# create interopolation mesh
M.IB.create_slice_mesh('z', 1e3, 20, y=1000., z=50., slice_name='rxair')

# interpolate fields on the observation line
M.IB.interpolate(['rxair_slice_z'])