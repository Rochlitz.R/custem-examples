#!/usr/bin/env python
# coding: utf-8

# # MarlimR3D - Comparison

# In[1]:


import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
# import loadmarlim

# data = xr.load_dataset('marlim_survey.nc', engine='h5netcdf')
# loadmarlim.create_survey(store_data=True)

data = xr.load_dataset('data/marlim_survey.nc', engine='h5netcdf')
#data = xr.load_dataset('ref/marlim_emg3d.nc', engine='h5netcdf')

# Receiver positions (reciprocity)
offs = data.src_x[::2] - data.rec_x
offs += 390275

# Line styles
colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
colors.pop(5)

def extract_line(d, n, b=None):
    data = getattr(d, n)
    # data = np.abs(data.data[::2, :, :] + 1j*data.data[1::2, :, :])
    data = data.data[::2, :, :] + 1j*data.data[1::2, :, :]

    if b is not None:
        comp = getattr(b, n)
        comp = np.abs(comp.data[::2, :, :] + 1j*comp.data[1::2, :, :])
        comp = comp.data[::2, :, :] + 1j*comp.data[1::2, :, :]
        data -= comp

    return data


il = 'data/inline.npy'
bs = 'data/broadside.npy'

fig, axs = plt.subplots(2, 3, figsize=(12, 7), sharex=True, sharey=False)

for iii, datname in enumerate(['data_bs', 'data_bs']):

    # Get absolute values of this line
    tdat = extract_line(data, datname)
    if iii == 0:
        export = np.load(bs)
    else:
        export = np.load(bs)

    # Loop over components Ex, Ey, Ez
    for ii, comp in enumerate(data.components.values[:3]):

        plt.sca(axs[iii, ii])
        if iii == 0:
            plt.title(f"{['Broadside'][iii]} -- {comp}")

        # Loop over frequencies
        for i, freq in enumerate(data.freqs.values[1::3]):

            if i == 0:
                i = 1
            elif i == 1:
                i = 4

            ref = np.load('./data/f_' + str(freq) + '_interpolated/'
                          'E_t_on_broadside_path_line_x.npy')[::-1] / 10.

            misfit1 = 200 * (np.abs(np.abs(export[ii, :, i]) -
                                    np.abs(tdat[:, i, ii]))) /\
                             (np.abs(export[ii, :, i]) +
                             np.abs(tdat[:, i, ii]))

            misfit2 = 200 * (np.abs(np.abs(ref[:, ii]) -
                                    np.abs(tdat[:, i, ii]))) /\
                              (np.abs(ref[:, ii]) +
                              np.abs(tdat[:, i, ii]))

            misfit3 = 200 * (np.abs(np.abs(export[ii, :, i]) -
                                    np.abs(ref[:, ii]))) /\
                              (np.abs(export[ii, :, i]) +
                              np.abs(ref[:, ii]))

            if iii == 0:
            # Plot this component/frequency
                if i == 1:
                    plt.axhline(1e-15, color='k', label='1e-15 V/m')
                plt.plot(offs[:102], np.abs(tdat[:102, i, ii]), '--', color=colors[i], lw=1., label=f"C&M {freq:4.3f} Hz")
                plt.plot(offs[102:], np.abs(tdat[102:, i, ii]), '--', color=colors[i], lw=1.)

                if i == 4:
                    plt.plot(offs[102:], np.abs(export[ii, 102:, i]), 'k:', lw=1., label='custEM RA')
                    plt.plot(offs[102:], np.abs(ref[102:, ii]), 'k-', lw=0.5, label=r'custEM $\mathcal{FD}$')
                else:
                    plt.plot(offs[102:], np.abs(export[ii, 102:, i]), 'k:', lw=1.)
                    plt.plot(offs[102:], np.abs(ref[102:, ii]), 'k-', lw=0.5)
                plt.plot(offs[:102], np.abs(export[ii, :102, i]), 'k:', lw=1.)
                plt.plot(offs[:102], np.abs(ref[:102, ii]), 'k-', lw=0.5)

                plt.grid('on')
                plt.yscale('log')
                plt.ylim([1e-18, 1e-10])
            else:
                plt.plot(offs[:102], misfit1[:102], '-', color=colors[i],
                         lw=1,
                         label=str(freq) + r' Hz (%), RA - C&M')
                plt.plot(offs[:102], misfit2[:102], ':', color=colors[i],
                         lw=1.5,
                         label=str(freq) + r' Hz (%), $\mathcal{FD}$ - C&M')
                plt.plot(offs[:102], misfit3[:102], '-', color=colors[i],
                         lw=0.3,
                         label=str(freq) + r' Hz (%), RA - $\mathcal{FD}$')
                plt.plot(offs[102:], misfit1[102:], '-', color=colors[i],
                         lw=1)
                plt.plot(offs[102:], misfit2[102:], ':', color=colors[i],
                         lw=1.5)
                plt.plot(offs[102:], misfit3[102:], '-', color=colors[i],
                         lw=0.3)

                plt.grid('on')
                plt.yscale('log')
                plt.ylim([1e-2, 1e2])

        if iii == 0:
            plt.ylabel('||E|| (V/m)')
        else:
            plt.ylabel('misfit (%)')
            plt.xlabel('position on broadside line (m)')

        if ii == 1:
            axs[iii, ii].set_yticklabels([])
            axs[iii, ii].minorticks_off()
            axs[iii, ii].set_ylabel('')
        if ii == 2:
            axs[iii, ii].yaxis.set_ticks_position('right')
            axs[iii, ii].yaxis.set_label_position('right')

    plt.legend(fontsize=7.6, ncol=2)
plt.tight_layout()
plt.savefig('plots/td_fd_misfits.pdf')
plt.show()

