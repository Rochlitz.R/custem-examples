# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import matplotlib
import numpy as np
#matplotlib.use('pdf')
from saem import CSEMSurvey
from saem.mt import MTData
from matplotlib import cm, rcParams
import matplotlib.pyplot as plt


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot data and final response misfit # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% import results and use plot utilities from SAEM class

# specify p
p = 12

# import results
#data = CSEMSurvey('data/Txy5em3Noise.npz', mode='T')
#data.loadResponse('inv_results/p12HighNoise_schleiz_inv_p12new')
# data = CSEMSurvey('data/Txy1em2Noise.npz', mode='T')
# data.loadResponse('data', response=0)
# data = CSEMSurvey('data/TxyHSNoise.npz', mode='T')
# data.loadResponse('data', response=1)
# data = CSEMSurvey('data/TxyAnomNoise.npz', mode='T')
# data.loadResponse('data', response=2)


# data = CSEMSurvey('data/TxyRSHSNoise.npz', mode='T')
# data.loadResponse('data', response=3)
# data2 = CSEMSurvey('data/TxyRRSHSNoise.npz', mode='T')
# data2[0].DATA[0] = data[0].RESP[1]
# data2[0].DATA[1] = data[0].RESP[0]
# data2.loadResponse('data', response=4)


data = CSEMSurvey('data/TxyRSAnomNoise.npz', mode='T')
data.loadResponse('data', response=8)
data2 = CSEMSurvey('data/TxyRRSAnomNoise.npz', mode='T')
data2[0].DATA[0] = data[0].RESP[1]
data2[0].DATA[1] = data[0].RESP[0]
data2.loadResponse('data', response=9)

f = 0
data2.showData(nf=f)
data2.showData(what='response', nf=f)
data2.showData(what='rmisfit', nf=f, log=False, alim=[-100., 100.])
# data.patches[0].rotate(np.deg2rad(-37.))
# data.patches[0].detectLines(np.linspace(-1900., 2200., 21), axis='y')
# data.patches[0].rotateBack()
# data.patches[0].showField(data.patches[0].line)


asd

for pi, patch in enumerate(data.patches):
    # patch.generateDataPDF(pdffile='p' + str(p) + '_linewise_misfit_.pdf',
    #                       mode='linefreqwise')
    patch.generateDataPDF(pdffile='p' + str(p) + '_data_patch_' +
                          str(pi) + '.pdf', mode='patchwise')
    # patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
    #                       str(pi) + '.pdf', what='wmisfit', log=False,
    #                       alim=[-2., 2.], mode='patchwise')
asd

# data = CSEMSurvey('data/ExyBxyzToPlot.npz')
# data.patches[0].cmp = [1, 1, 0]  # hack cmp detection for E-field data,
# data.patches[1].cmp = [1, 1, 0]  # hack cmp detection for E-field data,
# data.loadResults(dirname='inv_results/p' + str(p) + '_schleiz_inv_p' + str(p))

# patches[0] ... E-field Rx of Tx 0
# patches[1] ... E-field Rx of Tx 1
# patches[2] ... B-field Rx of Tx 0 (without Rx in vicinity of Tx)
# patches[3] ... B-field Rx of Tx 1 (without Rx in vicinity of Tx)
# patches[4] ... B-field Rx of Tx 0 in vicinity of Tx 1
# patches[5] ... B-field Rx of Tx 1 in vicinity of Tx 0

# %% generate pdf files

for pi, patch in enumerate(data.patches):
    if pi < 2:  # E-field surface Rx stations
        patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
                              str(pi) + '.pdf', mode='linefreqwise',
                              alim=[1e-7, 1e-3], field='E')
    else:  # B-field Rx in the air
        patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
                              str(pi) + '.pdf', mode='linefreqwise')