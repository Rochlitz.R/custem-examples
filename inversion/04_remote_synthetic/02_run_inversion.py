# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
from saem.tools import coverage

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #  run synthetic data inversion # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


pfname = 'TxyRemote'

# %% Apply error model

np.random.seed(99999)
err = 0.05        # relative error
noise_T = 5e-3    # related to expected tipper data amplitude ranges

# load data without noise
data = np.load('data/' + pfname + '_no_noise.npy')
# load geometry parameters for mesh generation
param = np.load('data/survey_parameters.npz', allow_pickle=True)

# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_T

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes
data[np.abs(data) < noise_T*0.5] = np.nan

# convert column vector to data structure required by inversion module
freqs = [6., 10., 20., 35., 60., 100., 200., 350., 600., 1000., 2000., 3500.]
cmps = [['Tx', 'Ty'], ['Tx', 'Ty']]
tx_ids = [[0, 1], [0, 1]]
mod = 'synth_data'
meshname = 'schleiz_rs_hs'
fop = MultiFWD(mod, meshname, freqs, cmps, tx_ids, approach='MT',
               remote_station=param['rs'].tolist())
fop.export_npz('data/' + pfname, data, abs_error)

# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly
p = 1
invmod = 'R'
invmesh = 'schleiz_remote'

# %% create mesh

rx_air = param['rx_air']

# define inversion area to be incorporated in the mesh
invpoly = np.array([[-3e3, -2e3, 0.],
                    [4e3, -2e3, 0.],
                    [4e3, 2e3, 0.],
                    [-3e3, 2e3, 0.]])

# shift and rotate inversion area to match footprint of Tx and Rx
invpoly = mu.rotate(invpoly, np.deg2rad(param['rot']))
invpoly[:, 0] += 800.
invpoly[:, 1] -= 400.

# create world
M = BlankWorld(name=invmesh,
               x_dim=[-1e5, 1e5],
               y_dim=[-1e5, 1e5],
               z_dim=[-1e5, 1e5],
               t_dir='data',
               topo=500.,
               easting_shift=float(param['xshift']),
               northing_shift=float(param['yshift']),
               inner_area_cell_size=6e3,
               preserve_edges=True,
               )

# build surface mesh
M.build_surface(insert_paths=mu.refine_rx(param['rs'], 1.))

# define inversion domain
M.add_inv_domains(-500., invpoly, x_frame=1.4e3, y_frame=1.6e3, z_frame=2e3,
                  cell_size=1e7)

# build surface mesh and extension to 3D
M.build_halfspace_mesh()

# shift airborne Rx coordinates to 60m height above triangulated surface
rx_air[:, 2] = M.get_topo_vals(rx_air, z=60.)

# add airborne Rx coordinates to parameter file and add refinement triangles
M.add_rx(rx_air)
rx_tri = mu.refine_rx(rx_air, 10., 30.)
M.add_paths(rx_tri)

# add remote station as extra patch (or not...)
M.add_rx(param['rs'])

# add boundary mesh and mesh generation with proper quality for p1 or p2
M.call_tetgen(tet_param='-pq1.6aA')

# %% define inversion parameter

sig_bg = 2e-3
max_iter = 21
synth_data = np.load('data/TxyRemote.npz', allow_pickle=True)

# %% set up forward operator
fop = MultiFWD(invmod, invmesh, approach='MT',
               saem_data=synth_data, sig_bg=sig_bg, p_fwd=p, max_procs=6,
               remote_station=param['rs'].tolist(), start_iter=0)
fop.setRegionProperties("*", limits=[1e-4, 1e0])

# %% set up inversion operator
inv = pg.Inversion(verbose=True)  # , debug=True)
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)

# %% run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=10., verbose=True,
                   startModel=fop.sig_0, maxIter=max_iter)

# save final conductivity vector
np.save(fop.inv_dir + 'inv_model.npy', invmodel)

# %% post-processing

# invmodel = np.load(inv.fop.inv_dir + 'inv_model.npy')

pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel
pgmesh['coverage'] = coverage(inv)
pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')
