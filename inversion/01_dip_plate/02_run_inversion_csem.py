# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
from saem.tools import coverage
from saem import CSEMSurvey


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #  run synthetic data inversion # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly
p = 1
lam = 10.
invmod = 'p' + str(p)
invmesh = 'schleiz_inv_p' + str(p)

# %% create mesh

# load geometry
param = np.load('data/survey_parameters.npz', allow_pickle=True)

inv_area = np.array([[-1100., -1100., 0.],
                     [1100., -1100., 0.],
                     [1100., 1100., 0.],
                     [-1100., 1100., 0.]])

dim = 1e4
M = BlankWorld(x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               name=invmesh,
               preserve_edges=True,
               )

M.build_surface(insert_line_tx=param['txs'])

M.add_inv_domains(-1000., inv_area, cell_size=1e7,
                  x_frame=2e3, y_frame=2e3, z_frame=2e3)

M.build_halfspace_mesh()

M.add_paths(param['rx_tri'])
M.add_rx(param['rx_all'])
for rx in param['rx_single']:
    M.add_rx(rx)

M.call_tetgen(tet_param='-pq1.6aA')

# %% define inversion parameter

sig_bg = 2e-3
max_iter = 21
synth_data = np.load('data/BxyzToImport.npz', allow_pickle=True)
toplot = CSEMSurvey('data/BxyzToPlot.npz')

# %% set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=synth_data,
               sig_bg=sig_bg, p_fwd=p, max_procs=6, start_iter=0)
fop.setRegionProperties("*", limits=[1e-4, 1e0])

# %% set up inversion operator
inv = pg.Inversion(verbose=True)  # , debug=True)
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)

# %% run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=lam, verbose=True,
                    startModel=fop.sig_0, maxIter=max_iter)

# save final conductivity vector
np.save(fop.inv_dir + 'final_inv_model.npy', invmodel)

# %% post-processing

# invmodel = np.load(inv.fop.inv_dir + 'final_inv_model.npy')

pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel
pgmesh['coverage'] = coverage(inv)
pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')
