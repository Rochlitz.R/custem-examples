# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


import numpy as np
import matplotlib.pyplot as plt
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import os
from saem import CSEMSurvey
import zipfile


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #  create synthetic data for inversion  # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# fix random seeed for reproducibility
np.random.seed(99999)

# %% mesh and survey parameter definitions

if os.path.isdir('data'):
    pass
else:
    os.makedirs('data')

# unzip digital elevation model
with zipfile.ZipFile("../../supplementsGJI/02_synthetic_example/" +
                     "data/dem_schleiz.zip","r") \
    as zip_ref:
    zip_ref.extractall("data")

# origin of computational domain
xshift = - 701980
yshift = - 5604726

# rotation for synthetic Rx grid to match approximately with Tx direction
rot = -37.

# define airborne Rx
xr = np.linspace(-2500., 3500., 51)
yr = np.linspace(-2000., 2000., 21)
rx_air = np.zeros((51 * 21, 3))

# create Rx grid
a = 0
for x in xr:
    for y in yr:
        rx_air[a, 0] = x
        rx_air[a, 1] = y
        a += 1

# rotate and shift Rx grid above Tx
rx_air = mu.rotate(rx_air, np.deg2rad(rot))
rx_air[:, 0] += 800.
rx_air[:, 1] -= 400.


# locate surface reference station somewhere within Rx grid
rx_base = [[0., 0., -1.]]
rx_tri_base = mu.refine_rx(rx_base, 1.)


# %% mesh generation
# define poylgones for anomalies which outcrop at the surface
anom1 = np.array([[-2000., -500., 0.],
                  [-500., -1500., 0.],
                  [0., -1000., 0.],
                  [-800., 0., 0.],
                  [-1200., 200., 0.]])
anom2 = np.array([[-1200., 200., 0.],
                  [-800., 0., 0.],
                  [-300., 400., 0.],
                  [-500., 600., 0.]])
anom3 = np.array([[-1000., 800., 0.],
                  [300., 500., 0.],
                  [500., 1000., 0.],
                  [-800., 1600., 0.]])
anom4 = np.array([[1200., -3200., 0.],
                  [2200., -3600., 0.],
                  [3800., -400., 0.],
                  [3600., 200., 0.]])

# create world
M = BlankWorld(name='schleiz_tipper_remote',
               x_dim=[-5e3, 6e3],
               y_dim=[-6e3, 5e3],
               z_dim=[-5e3, 5e3],
               t_dir='data',
               topo='dem_25.asc',
               easting_shift=xshift,
               northing_shift=yshift,
               inner_area_size=[3400., 3000.],
               inner_area_shift=[800., -800.],
               inner_area='box',
               inner_area_cell_size=2e3,
               outer_area_cell_size=1e4,
               split_subsurface_anomalies=[True, False, False, False],
               z_approx=500.,
               preserve_edges=True,
               )

# define surface mesh
M.build_surface(insert_paths=rx_tri_base)

# define anomaly outcrops
d = [-600., -200., -2600., -1200.]
M.add_surface_anomaly(insert_paths=[anom1, anom2, anom3, anom4],
                      depths=d,
                      cell_sizes=[1e7, 1e6, 1e7, 1e7],
                      dips=[0., 0., 30., 50.],
                      dip_azimuths=[0., 0., 0., -70.],
                      split_depths=[[-200., -500., -1000.],
                                    [-200.],
                                    [-500.],
                                    [-800.]])

# build surface mesh and extension to 3D
M.build_halfspace_mesh()

# add 5th anomaly at greater depths in the center
M.add_plate(dx=2200., dy=600., dz=400., origin=[1000.0, -1200.0, -400.0],
            dip=30., dip_azimuth=rot-90., cell_size=1e6)

# shift airborne Rx coordinates to 60m height above triangulated surface
rx_air[:, 2] = M.get_topo_vals(rx_air, z=60.)

# add airborne Rx coordinates to parameter file and add refinement triangles
M.add_rx(rx_air)
rx_tri = mu.refine_rx(rx_air, 10., 30.)
M.add_paths(rx_tri)

# add boundary mesh and mesh generation
M.extend_world(10., 10., 10.)
M.call_tetgen(tet_param='-pq1.4aA', suppress='')

# %% save survey parameters to import for inversion mesh generation

np.savez('data/survey_parameters.npz',
         xshift=xshift,
         yshift=yshift,
         rot=rot,
         rx_air=rx_air,
         rx_base=rx_base,
         rx_tri_base=rx_tri_base)


# %% set synthetic data generation parameters

err = 0.05        #                           mu_0         nT
noise_T = 1e-2    # related to expected tipper data amplitude ranges

mesh = 'schleiz_tipper_remote'
p_fwd = 2         # polynomial order for forward modeling
mod = 'synth_data'
n_cores=64        # total number of cores used
min_freqs = None  # set to 2, 3 or 4 to trade time vs. RAM requirements,
#                 # e.g., 3 means each thread calculates 3 freqs,
#                 # so time is tripled but RAM requirements are only 1/3

freqs = [8., 16., 24., 32., 64., 96., 128., 256., 512., 768., 1024.]
cmps = [['Tx', 'Ty']]
tx_ids = [[0, 1]]

# for anomaly markers 2, 3, 4, 5, 6, respectively
sig_m = [5e-4, 5e-4, 5e-4, 2e-2, 2e-4, 5e-2, 1e-1]
# for marker 1, surrounding halfspace
sig_bg = 2e-3


# %% create synthetic data

pfname = 'Tremote'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, approach='MT', min_freqs=min_freqs,
               n_cores=n_cores, remote_station=rx_base)
data = fop.response(sig_m)
np.save('data/Tremote_no_noise.npy', data)

# %%
data = np.load('data/Tremote_no_noise.npy')

# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_T

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes
# data[np.abs(data) < noise_T*0.5] = np.nan

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)