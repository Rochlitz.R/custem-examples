# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


import numpy as np
import os
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #  create synthetic data for inversion  # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# fix random seeed for reproducibility
np.random.seed(99999)
mod = '00'
mesh = 'minimalistic'

# %% mesh and survey parameter definitions

# define airborne Rx
n = 3
rx_air = mu.rx_grid([-100., 100., n], [-100., 100., n], z=60.)
rx_surf = mu.line_x(60., 80., n_segs=7)
txs = [mu.line_y(-0.95e2, 0.55e2, n_segs=21, x=-1.05e3)]
       #mu.line_x(-0.95e2, 0.55e2, n_segs=21)]

rx_tris = mu.refine_rx(rx_surf, r=2.)
M = BlankWorld(name=mesh)

M.build_surface(insert_line_tx=txs,
                insert_paths=rx_tris)
M.build_halfspace_mesh()

M.add_plate(dx=100., dy=100., dz=100., origin=[-23., 21., -120.0],
            dip=70., dip_azimuth=33.,
            cell_size=1e6)

#rx_tri = mu.refine_rx(rx_air, r=10.)
#M.add_paths(rx_tri)
#M.add_rx(rx_air)
M.add_rx(rx_surf)

M.call_tetgen(tet_param='-pq1.4/20aA')

if os.path.isdir('data'):
    pass
else:
    os.makedirs('data')
np.savez('data/survey_parameters.npz',
         txs=np.array(txs, dtype=object),
         #rx_air=rx_air,
         rx_surf=rx_surf,
         rx_tris=rx_tris)

#%% set synthetic data generation parameters

err = 0.05        # relative error
noise_B = 1e-3    # related to expected tipper data amplitude ranges
p_fwd = 2         # polynomial order for forward modeling
n_cores = 24      # total number of cores used
min_freqs = None  # set to 2, 3 or 4 to trade time vs. RAM requirements,
#                 # e.g., 3 means each thread calculates 3 freqs,
#                 # so time is tripled but RAM requirements are only 1/3

freqs = [100.]

# suited for synthetic inversion studies to summarize common Rx and Tx
cmps = [['Bx', 'By', 'Bz']] #, ['Ex', 'Ey', 'Bx', 'By', 'Bz']]
tx_ids = [[0]] #, 1], [0]]

# for anomaly marker
sig_m = [1e-1]
# for marker 1, surrounding halfspace
sig_bg = 1e-3


# %% create synthetic CSEM data

pfname = 'Bxyz'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores)
data = fop.response(sig_m)
np.save('data/' + pfname + '_no_noise.npy', data)

data = np.load('data/' + pfname + '_no_noise.npy')
# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_B

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes for E_real, B_real, E_imag, B_imag
data[np.abs(data) < noise_B*0.5] = np.nan

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)



# # %% create synthetic MT data

# pfname = 'ZT'
# cmps = [['Tx', 'Ty'], ['Zxx', 'Zxy', 'Zyx', 'Zyy']]
# tx_ids = [[0, 1], [0, 1]]
# fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
#                p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores)
# data = fop.response(sig_m)
# np.save('data/' + pfname + '_no_noise.npy', data)

# data = np.load('data/' + pfname + '_no_noise.npy')
# # define abs_error for all recordoings (B fields)
# abs_error = np.abs(data) * err + noise_B

# # add noise
# data += np.random.randn(len(data)) * abs_error

# # remove very weak amplitudes for E_real, B_real, E_imag, B_imag
# data[np.abs(data) < noise_B*0.5] = np.nan

# # convert column vector to data structure required by inversion module
# fop.export_npz('data/' + pfname, data, abs_error)