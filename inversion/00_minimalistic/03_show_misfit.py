# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import matplotlib
# matplotlib.use('pdf')
from saem import CSEMSurvey
from matplotlib import cm, rcParams
import matplotlib.pyplot as plt
import numpy as np


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot data and final response misfit # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% import results and use plot utilities from SAEM class

# import results
data = CSEMSurvey('data/BxyzToPlot.npz')
data2 = CSEMSurvey('data/BxyzIPToPlot.npz')
data.patches[0].RESP = data2.patches[0].DATA
data.patches[1].RESP = data2.patches[1].DATA

# %%
#data.patches[0].detectLines(np.linspace(-1200., 1200., 25), axis='y')
#data.patches[0].detectLines(np.linspace(-3000., 3000., 61), axis='x')
#data.patches[0].showField(data.patches[0].line)

#data.patches[0].showLineFreq(nf=2, line=23, axis='y')
#data.patches[0].showLineDataMat(line=23, axis='y')
#data.patches[0].showLineData(line=23, axis='y')
data.patches[0].showPatchData(nf=3, what='wmisfit', log=False)
asd

# %% generate pdf files

for pi, patch in enumerate(data.patches):
    patch.generateDataPDF(pdffile='misfit_IP_noIP_Tx' + str(pi+1) + '.pdf', mode='patchwise',
                          what='rmisfit', log=False)